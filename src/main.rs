use reqwest;
use std::fs;
use std::str;
use std::error::Error;
use std::process::exit;
use clap::{App, Arg};
use reqwest::multipart;
use serde::{Deserialize, Serialize};
use http_auth_basic::Credentials;

#[derive(Deserialize)]
struct ImageResponse {
    id: String,
    url: String,
}


#[derive(Debug, Serialize, Deserialize)]
struct TootRequest{
    status: String,
    media_ids: Vec<String>,
    in_reply_to_id: String
}

#[derive(Deserialize)]
struct TootResponse {
    id: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = App::new("mastodon-cli")
        .version("0.1")
        .about("Mastodon console client")
        .arg(Arg::with_name("instance")
            .short("i")
            .long("instance")
            .help("your mastodon instance")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("auth")
            .short("a")
            .long("auth")
            .help("your apiToken")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("user")
            .short("u")
            .long("user")
            .help("your username")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("password")
            .short("p")
            .long("password")
            .help("your password")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("text")
            .help("the text")
            .short("t")
            .long("text")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("image")
            .help("attach an image")
            .short("g")
            .long("image")
            .takes_value(true)
            .required(false)
            .multiple(true))
        .arg(Arg::with_name("dir_images")
            .help("attach a directory of images")
            .short("dg")
            .long("dir")
            .takes_value(true)
            .required(false)
            .multiple(false))
        .arg(Arg::with_name("file")
            .help("a file")
            .short("f")
            .long("file")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("debug")
            .help("dont send only see how it works")
            .short("d")
            .long("debug")
            .takes_value(false)
            .required(false))
        .arg(Arg::with_name("size")
            .help("max size of every toot when file is used")
            .short("s")
            .long("size")
            .takes_value(true)
            .required(false))
        .get_matches();

    let mut size_toot = 500;
    if args.is_present("size") {
        size_toot = args.value_of("size").unwrap().parse().unwrap();
    };

    let authorization;

    if args.is_present("auth") {
        let auth = args.value_of("auth").unwrap();
        authorization = ["Bearer", auth].join(" ")
    }else
        if args.is_present("user") && args.is_present("password") {
            let credentials = Credentials::new(args.value_of("user").unwrap(), args.value_of("password").unwrap());
            authorization = credentials.as_http_header();
        }
        else {
            println!("Auth required");
            exit(-1);
        }

    let instance = args.value_of("instance").unwrap();

    let mut media_id:Vec<String> = Vec::new();
    let debug = args.is_present("debug");

    if args.is_present("image") {
        let images: Vec<&str> = args.values_of("image").unwrap().collect();
        for image in images {
            let form = multipart::Form::new()
                .file("file", image)?;

            let client = reqwest::Client::new();
            let res: ImageResponse = client.post(&["https://", instance, "/api/v1/media"].join(""))
                .header("Authorization", authorization.as_str())
                .multipart(form)
                .send()?
                .json()?;
            media_id.push(res.id);
        }
    }

    if args.is_present("dir_images") {
        let directory = args.value_of("dir_images").unwrap();
        for image in fs::read_dir(directory).unwrap() {
            let form = multipart::Form::new()
                .file("file", image.unwrap().path())?;

            let client = reqwest::Client::new();
            let res: ImageResponse = client.post(&["https://", instance, "/api/v1/media"].join(""))
                .header("Authorization", authorization.as_str())
                .multipart(form)
                .send()?
                .json()?;
            media_id.push(res.id);
        }
    }


    if args.is_present("text") {
        let request = TootRequest{
            status: args.value_of("text").unwrap().into(),
            media_ids: media_id.into(),
            in_reply_to_id: "".to_string(),
        };

        let client = reqwest::Client::new();
        let res:TootResponse = client.post(&["https://",instance,"/api/v1/statuses"].join(""))
            .header("Authorization", authorization.as_str())
            .json(&request)
            .send()?
            .json()?;
        print!("{}",res.id);
    }else{
        if args.is_present("file") {
            let filename = args.value_of("file").unwrap();
            let text : String = fs::read_to_string(filename)?.parse()?;
            let client = reqwest::Client::new();

            let subs = text.split("\n");
            
            let mut toot: String = "".to_owned();
            let mut last_id: String = "".to_owned();
            let mut toots = Vec::new();

            for w in subs {
                let send = w.starts_with(">>>>") || toot.len()+w.len() > size_toot;
                if send {
                    toots.push(toot);
                    toot = "".to_owned();
                }
                if  w.len() == 0 {
                    toot.push_str("\n");
                }else{
                    if !w.starts_with(">>>>") {
                        toot.push_str(w);
                        toot.push_str("\n");
                    }
                }
            }
            toots.push(toot);

            if debug {
                let mut txt: String = "".to_owned();
                for t in toots {
                    txt.push_str(t.as_str());
                    txt.push_str(">>>>\n");
                }
                fs::write(filename, txt).expect("TODO: panic message");
            }else{
                for t in toots{
                    let request = TootRequest{
                        status: t,
                        media_ids: Vec::new(),
                        in_reply_to_id: last_id
                    };
                    let res:TootResponse = client.post(&["https://",instance,"/api/v1/statuses"].join(""))
                        .header("Authorization", authorization.as_str())
                        .json(&request)
                        .send()?
                        .json()?;
                    last_id = res.id.to_owned();
                }
            }

        }
    }
    Ok(())
}

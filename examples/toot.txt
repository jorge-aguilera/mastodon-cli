Hola

soy un toot enviado desde mastodon-cli, una herramienta de consola que permite enviar
tanto toots sueltos como desde un fichero.

Lo primero que necesitas crear en tu instancia es un Token (una cadena de letras y números)
por lo que tienes que ir a tu perfil y en "desarrollo" crear un token. Guardalo y no lo 
compartas porque es la manera de identificarte y si lo compartes podrian usarlo para hacer
el mal, que la gente es mu joeputa

También necesitas conocer el nombre de tu instancia. Por ejemplo la mía es "mastodon.madrid"

Fijate que sólo necesitas el nombre, el resto de https y tal lo añade mastodon-cli

Una vez tengas estas dos cosas ya puedes usarlo desde consola (o programar su ejecución)

----

mastodon-cli te permite enviar un toot:

$ mastodon-cli --auth token --instance mastodon.madrid --text "Hola caracola"

o enviar un toot con imagen:

$ mastodon-cli --auth token --instance mastodon.madrid --text "Hola caracola" --image gatitos.png

o partir un fichero en toots y enviarlos como hilo:

$ mastodon-cli --auth token --instance mastodon.madrid --file mi-hilo.txt 
